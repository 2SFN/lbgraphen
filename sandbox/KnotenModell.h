//
// Created by didi1 on 23.06.2020.
//

#ifndef LBGRAPHEN_KNOTENMODELL_H
#define LBGRAPHEN_KNOTENMODELL_H

#include <iostream>
#include "../graphen/Graph.h"

/**
 * Einfache Test-Klasse für das angepasste Graphen- und Knotenmodell.
 */

class KnotenModell {
public:
    static void test() {
        // Graph erstellen
        Graph g(5);

        // Matrix ausgeben
        Graph_Debug(g);
        std::cout << "\n\n";

        // Kanten einfügen
        Insert_Edge(g, 1, 2);
        Insert_Edge(g, 3, 0);

        // Matrix ausgeben
        Graph_Debug(g);
    }
};


#endif //LBGRAPHEN_KNOTENMODELL_H
