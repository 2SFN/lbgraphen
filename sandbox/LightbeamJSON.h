//
// Created by i on 16.06.2020.
//

#ifndef LBGRAPHEN_LIGHTBEAMJSON_H
#define LBGRAPHEN_LIGHTBEAMJSON_H

#include <iostream>
#include <fstream>
#include "../json/single_include/nlohmann/json.hpp"

/**
 * Einfache Test-Klasse für die Verarbeitung von Lightbeam JSON-Daten.
 */

class LightbeamJSON {
    private:

    public:
        static void read(){
            // read a JSON file
            std::ifstream i("res/lightbeam.json");
            nlohmann::json j;
            i >> j;
            std::cout << j["a.bf-tools.net"];
        }

};


#endif //LBGRAPHEN_LIGHTBEAMJSON_H
