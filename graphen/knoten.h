#ifndef KNOTEN_H
#define KNOTEN_H

#include <climits>
#include <string>
#include <utility>

/**
 * Knoten-Modell mit Distanz, Farbe, Vorgänger und Bezeichnung.
 */

using namespace std;

enum KnotenFarbe {
    WEISS,
    GRAU,
    SCHWARZ,
    /**
     * Schwarz >> Orange
     *
     * Hilfreich für weitere Iterationen
     * der Breitensuche.
     */
    ORANGE
};
#define NIL -1
#define UNENDLICH INT_MAX

/**
 * @brief Klasse Knoten
 */
class Knoten {
public:

    Knoten(string name) : name(std::move(name)) {
        d = UNENDLICH;
        vorgaenger = NIL;
        farbe = WEISS;
    }

    KnotenFarbe farbe;  // Farbmarkierung für Breitensuche
    int d;              // Distanz zum Startknoten bei der Breitensuche
    int vorgaenger;     // Pi
    string name;        // Name des Knotens (Erweiterung OD)

    Knoten() {
        d = UNENDLICH;
        vorgaenger = NIL;
        farbe = WEISS;
        name = "";
    }
};

#endif // KNOTEN_H
