#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err60-cpp"
#pragma ide diagnostic ignored "cppcoreguidelines-narrowing-conversions"
//
// Created by SFN on 17.05.2020.
//

#include <iostream>
#include <vector>
#include <stdexcept>
#include <fstream>
#include "Graph.h"
#include "../util/queue.h"
#include "../util/RandomTool.h"
#include "../util/Point.h"

using namespace std;

void PRINT_ADJ(Graph &g) {
    int knoten = g.knotenzahl;

    for (int j = 0; j < knoten; ++j) {
        for (int i = 0; i < knoten; ++i) {
            cout << ((int) *(g.Adj(i,j))) << " ";
        }
        cout << endl;
    }
}

void Graph_Debug(Graph &g) {
    PRINT_ADJ(g);
}

void BFS(Graph &G, int s) {
    Queue Q;

    // Anfangsknoten anpassen
    G.V[s].farbe = GRAU;
    G.V[s].d = 0;
    G.V[s].vorgaenger = NIL;

    // Ersten Knoten einreihen
    ENQUEUE(Q, s);

    while(Queue_NotEmpty(Q)) {
        int u = DEQUEUE(Q);

        for(int v = 1; v < G.knotenzahl; v++) {
            if(*(G.Adj(v, u)) == 1 && G.V[v].farbe == WEISS) {
                G.V[v].farbe = GRAU;
                G.V[v].d = G.V[u].d + 1;
                G.V[v].vorgaenger = u;
                ENQUEUE(Q, v);
            }
        }

        G.V[u].farbe = SCHWARZ;
    }
}

void Insert_Edge(Graph &G, unsigned int u, unsigned int v) {
    // Parameter validieren
    if(u > G.knotenzahl - 1 || v > G.knotenzahl - 1)
        throw invalid_argument("Invalid edge index.");

    // Adjazenz erzwingen
    *(G.Adj(u, v)) = 1;
    *(G.Adj(v, u)) = 1;
}

void SaveGraph(ostream &os, Graph &G) {
    os << "G " << G.knotenzahl << "\n";

    // Knoten schreiben
    for (int i = 0; i < G.knotenzahl; ++i) {
        os << "V " << (i+1) << " \"" << G.V[i].name << "\"\n";
    }

    // Kanten schreiben
    for (int j = 0; j < G.knotenzahl; ++j) {
        for (int k = 0; k < G.knotenzahl; ++k) {
            if(*(G.Adj(j, k)) == 1)
            os << "E " << (j+1) << " " << (k+1) << " 1\n";
        }
    }
}


#pragma clang diagnostic pop