#ifndef GRAPH_H
#define GRAPH_H

/**
 * Struktur eines Graphen.
 *
 * Version mit Anpassungen, um Grpahen mit dynamischer
 * Knotenzahl zu ermöglichen.
 */

#include <iostream>
#include <memory.h>
#include "knoten.h"

using namespace std;

/**
 * @brief Graph
 */
class Graph
{
public:
    Knoten* V;
    int knotenzahl;

    /**
     * Mehrdimensionales Array durch einfachen Array-Pointer
     * ersetzt, um die Speicherreservierung zu erleichtern.
     */
    unsigned char* AdjInt;

    Graph(int knotenzahl = 0) {
        this->knotenzahl = knotenzahl;
        this->V = new Knoten[knotenzahl];
        this->AdjInt = new unsigned char[knotenzahl*knotenzahl];

        // Initialisiere mehrdimensionales Array mit 0
        memset(AdjInt, 0, sizeof(unsigned char) * knotenzahl * knotenzahl);
    };

    virtual ~Graph() {
        delete [] this->V;
        delete [] this->AdjInt;
    }

    /**
     * Durch die Repräsentation der Matrix als eindimensionales
     * Array dient diese Funktion als Ersatz zum Zugriff über die
     * Indizes i und j.
     *
     * @param i Index
     * @param j Index
     * @return Referenz
     */
    unsigned char* Adj(int i, int j) {
        // ary[i*sizeY+j]
        return &(AdjInt[i * knotenzahl + j]);
    }
};

/**
 * @brief BFS - Breitensuche auf einem Graphen G gemäß Pseudocode
 * Kapitel 22.2 Breitensuche auf Seite 605
 * @param G Graph
 * @param s Index des Startknotens
 */
void BFS(Graph &G, int s);

/**
 * @brief SaveGraph Schreibt die Graphdaten in den geöffneten ostream
 * (oder abgeleitet: iostream, ofstream, ostringstream)
 * @param os geöffneter output stream
 * @param G Graph, der gespeichert wird
  */
void SaveGraph(ostream &os, Graph &G);

/**
 * @brief Insert_Edge Fügt eine ungerichtete Kante von Knoten u nach Knoten v ein
 * @param G Graph, in dem die Kante hinzugefügt wird
 * @param u Startknoten der ungerichteten Kante
 * @param v Endknoten der ungerichteten Kante
 */
void Insert_Edge(Graph &G, unsigned int u, unsigned int v);

/**
 * @brief Graph_Debug
 * @param G Auszugebender Graph
 */
void Graph_Debug(Graph &G);

/**
 * Einfache (Debug-) Ausgabe der Adjazenzmatrix
 * eines Graphen.
 *
 * @param g Graph.
 */
void PRINT_ADJ(Graph &g);


#endif // GRAPH_H