//
// Created by AdrianGrubba on 23.06.2020.
//

#ifndef LBGRAPHEN_LBPARSER_H
#define LBGRAPHEN_LBPARSER_H

#include <iostream>
#include <fstream>
#include "../graphen/Graph.h"
#include "../graphen/knoten.h"
#include "../json/single_include/nlohmann/json.hpp"

/**
 * Liest LightBeam JSON-Files ein und konvertiert sie
 * in einen Graphen.
 */
class LBParser {

public:
    static Graph* einlesen(const string& dateiname) {
        // JSON-Daten einlesen
        nlohmann::json* json = dateiJSON(dateiname);

        // Hostnames ermitteln
        auto* hostnames = eindeutigeHosts(*json);

        // Graph erzeugen
        Graph* g = graphAusHostnames(hostnames);

        // Kanten ergänzen
        kantenErmitteln(g, json);

        return g;
    }

private:
    /**
     * Liest eine .json Datei in eine JSON-Objekt ein.
     *
     * @param dateiname Dateiname.
     * @return Verarbeitetes JSON.
     */
    static nlohmann::json* dateiJSON(const string& dateiname) {
        // Datei einlesen
        std::ifstream i(dateiname);

        // Als JSON Objekt parsen
        auto* j = new nlohmann::json();
        i >> *j;

        return j;
    }

    /**
     * Erstellt aus einem JSON-Objekt eine Liste von
     * _unterschiedlichen_ Hostnames.
     *
     * @param l
     * @return Liste von Hosts.
     */
    static vector<string>* eindeutigeHosts(nlohmann::json& l) {
        // Neue Liste
        auto* hosts = new vector<string>();

        for(nlohmann::json current : l) {
            if(current.contains("hostname")) {
                if(!(std::find(hosts->begin(), hosts->end(),
                        current["hostname"]) != hosts->end())) {
                    hosts->push_back(current["hostname"]);
                }
            }
        }

        return hosts;
    }

    /**
     * Erstelle Grpahen aus Liste von Hosts.
     * 
     * @param hostnames Liste von Hosts.
     * @return Graph.
     */
    static Graph* graphAusHostnames(vector<string>* hostnames) {
        auto* g = new Graph(hostnames->size());

        for(int i = 0; i < hostnames->size(); i++)
            g->V[i] = *(new Knoten((*hostnames)[i]));

        return g;
    }

    /**
     * Liest die Elemente aus dem "thirdParties"-Feld eines Eintrags
     * und ergänzt die Kanten im Grpahen.
     *
     * @param g Graph
     * @param json Lightbeam-JSON
     */
    static void kantenErmitteln(Graph* g, nlohmann::json* json) {
        for(int i = 0; i < g->knotenzahl; i++)
            for(int j = 0; j < g->knotenzahl; j++) {
                // Third-Party Array
                vector<string> tps = (*json)[ g->V[j].name ][ "thirdParties" ];

                for(auto & tp : tps)
                    if(tp == g->V[i].name)
                        Insert_Edge(*g, i, j);
            }
    }

};


#endif //LBGRAPHEN_LBPARSER_H
