//
// Created by SFN on 05.07.2020.
//

#ifndef LBGRAPHEN_ZUSAMMENHANGSKOMPONENTEN_H
#define LBGRAPHEN_ZUSAMMENHANGSKOMPONENTEN_H

/**
 * Klasse für Algorithmen und Hilfsfunktionen zur Ermittlung
 * der Zusammenhangskomponenten eines Graphen.
 */

#include "../graphen/Graph.h"
#include "../graphen/knoten.h"

class Zusammenhangskomponenten {
public:
    /**
     * Findet einzelne Komponenten eines Graphen und gibt diese
     * einzeln isoliert in einer Liste zurúck.
     *
     * @param g Graph
     * @return Liste einzelner Komponenten
     */
    static vector<Graph *> *gruppenFinden(Graph *g) {
        // Ergebnis-Liste
        auto *res = new vector<Graph *>();

        // Breitensuche für alle unbekannten Knoten
        int start;
        while((start = naechsterUnbekannter(g)) >= 0) {
            // Breitensuche ausführen
            BFS(*g, start);

            // Graph kopieren
            res->push_back(ergebnisKopie(g));

            // Schwarz >> Orange
            schwarzZuOrange(g);
        }

        return res;
    }

private:
    /**
     * Findet den Index des nächsten nicht entdeckten Knotens.
     *
     * @param g Graph
     * @return Index oder -1
     */
    static int naechsterUnbekannter(Graph* g) {
        for (int i = 0; i < g->knotenzahl; ++i) {
            if(g->V[i].farbe == WEISS)
                return i;
        }

        return -1;
    }

    /**
     * Ändert nach dem Isolieren alle entdekcten Knoten (Schwarz)
     * zu Orange.
     *
     * @param g Graph
     */
    static void schwarzZuOrange(Graph* g) {
        for (int i = 0; i < g->knotenzahl; ++i) {
            if(g->V[i].farbe == SCHWARZ)
                g->V[i].farbe = ORANGE;
        }
    }

    /**
     * Erstellt eine isolierte Kopie einer einzelnen Komponente
     * eines Grpahen.
     *
     * @param g Grpah (nach der Breitensuche)
     * @return Isolierte, eigenständige Kopie des Ergebnisses.
     */
    static Graph* ergebnisKopie(Graph* g) {
        // Zusammenhängende Hostnames ermitteln
        vector<int> hosts;
        for (int i = 0; i < g->knotenzahl; ++i) {
            if(g->V[i].farbe == SCHWARZ)
                hosts.push_back(i);
        }

        // Graph erstellen
        auto* res = new Graph(hosts.size());

        // Hostnames kopieren
        for (int j = 0; j < hosts.size(); ++j) {
            res->V[j].name = g->V[hosts[j]].name;

            // Adjazenzmatrix nachbilden
            for (int k = 0; k < hosts.size(); ++k) {
                // Originale Matrix prüfen
                if(*(g->Adj(hosts[j], hosts[k])) == 1)
                    // Kopie ergänzen
                    Insert_Edge(*res, j, k);
            }
        }

        return res;
    }
};


#endif //LBGRAPHEN_ZUSAMMENHANGSKOMPONENTEN_H
