/**
 * Implementierung einer Queue.
 */

#include "queue.h"
#include <iostream>

void ENQUEUE(Queue &Q, int x) {
    if( (Q.kopf == (Q.ende + 1)) ||
        (Q.kopf == 1 && Q.ende == Q.laenge) )
        throw "Queue overflow.";

    Q.Q[Q.ende] = x;
    if(Q.ende == Q.laenge)
        Q.ende = 1;
    else Q.ende++;
}

int DEQUEUE(Queue &Q) {
    if(Q.kopf == Q.ende)
        throw "Queue empty.";

    int x = Q.Q[Q.kopf];
    if(Q.kopf == Q.laenge)
        Q.kopf = 1;
    else
        Q.kopf++;

    return x;
}

int Queue_Empty(Queue &Q) {
    return Q.kopf == Q.ende;
}

int Queue_NotEmpty(Queue &Q) {
    return Q.kopf != Q.ende;
}

void Queue_Debug(Queue &Q) {
    std::cout << "[ ";

    if (Queue_NotEmpty(Q)) {
        int pos = Q.kopf;
        while(pos != Q.ende) {
            std::cout << " " << Q.Q[pos] << ",";

            if(pos == Q.laenge)
                pos = 1;
            else
                pos++;
        }
    }

    std::cout << "\b ]" << std::endl;
}