//
// Created by SFN on 28.05.2020.
//

#ifndef P2GRAPHEN_POINT_H
#define P2GRAPHEN_POINT_H


class Point
{
public:
    int x;

    Point() : x(0), y(0) {}
    Point(int x, int y) : x(x), y(y) {}

    int y;
};



#endif //P2GRAPHEN_POINT_H
