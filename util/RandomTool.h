//
// Created by SFN on 24.05.2020.
//

#ifndef C2S05_RANDOMTOOL_H
#define C2S05_RANDOMTOOL_H

#include <ctime>
#include <random>

class RandomTool {
public:
    static void initRandomTool() {
        srand(time(nullptr) + 0xc0ffee);
    }

/**
 * Generates a random float value between min and max.
 * (Both limits are included).
 *
 * @param min Lower limit
 * @param max Upper limit
 * @return Random float value.
 */
    static float getRandomFloat(float min, float max) {
        return (min + (float(rand())) / (RAND_MAX / (max - min)));
    }

/**
 * Generates a random integer value between min and max.
 * (Both limits are included).
 *
 * @param min Lower limit
 * @param max Upper limit
 * @return Random integer value.
 */
    static int getRandomInt(int min, int max) {
        return (rand() % (max - min + 1) + min);
    }
};


#endif //C2S05_RANDOMTOOL_H
