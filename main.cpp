#include <iostream>
#include <exception>
#include "json/single_include/nlohmann/json.hpp"
#include "lightbeam/LBParser.h"
#include "lightbeam/Zusammenhangskomponenten.h"

/**
 * Hauptanwendung. Konvertiert LightBeam JSON-File in das .gdi-Format.
 *
 * @param argc Anzahl Argumente.
 * @param argv Inhalt Argumente.
 * @return 0.
 */
int main(int argc, char** argv) {
    try {
        // Dateinamen aus Argumenten
        std::vector<std::string> args(argv, argv + argc);

        if(args.size() != 3)
            throw "Bitte Eingabe- und Ausgabe-Dateinamen angeben";

        std::string const &fileIn = args[1];
        std::string const &fileOut = args[2];

        if(fileIn.length() < 1 || fileOut.length() < 1)
            throw "Ein-/Ausgabe Dateinamen falsch!";

        // Einlesen
        Graph *original = LBParser::einlesen(fileIn);

        // Ausgeben
        ofstream os;
        os.open(fileOut, ofstream::out);
        SaveGraph(os, *original);
        os.close();

    } catch (const std::exception &e) {
        std::cerr << "Ein Fehler ist aufgetreten:\n" << e.what();
    } catch (const char* e) {
        std::cerr << "Ein Fehler ist aufgetreten:\n" << e;
    } catch (...) {
        std::cout << "Ein Fehler ist aufgetreten. Argumente pruefen und erneut versuchen.";
    }

    return 0;
}
